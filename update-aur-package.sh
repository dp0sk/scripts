#!/bin/bash

pkgname="$(echo $1 | sed 's/.*\///' | sed 's/\ .*//')"

name=$pkgname

version="$(wget -q -O - https://gitlab.com/api/v4/projects/schmiddi-on-mobile%2F${name}/releases/permalink/latest | sed 's/.*"tag_name":"//' | sed 's/",".*//')"

pkgver="$version"

previous_version=$(cat "$1/PKGBUILD" | grep pkgver= | sed 's/pkgver=//')

if [[ $version == "v"* ]]; then
	pkgver="$(echo $version | sed 's/v//')"

	previous_version="v$previous_version"
fi

sed -i "s/pkgver=.*/pkgver=$pkgver/" "$1/PKGBUILD"
sed -i "s/pkgver =.*/pkgver = $pkgver/" "$1/.SRCINFO"

sed -i "s/source = https:\/\/gitlab.com\/schmiddi-on-mobile\/$name\/-\/archive.*/source = https:\/\/gitlab.com\/schmiddi-on-mobile\/$name\/-\/archive\/$version\/$name-$version.tar.gz/" "$1/.SRCINFO"

sha256sum="$(wget https://gitlab.com/schmiddi-on-mobile/$name/-/archive/$version/$name-$version.tar.gz && sha256sum $name-$version.tar.gz | sed 's/\ .*//' && rm $name-$version.tar.gz)"

previous_sha256sum="$(wget https://gitlab.com/schmiddi-on-mobile/$name/-/archive/$previous_version/$name-$previous_version.tar.gz && sha256sum $name-$previous_version.tar.gz | sed 's/\ .*//' && rm $name-$previous_version.tar.gz)"

sed -i "s/$previous_sha256sum/$sha256sum/" "$1/PKGBUILD"
sed -i "s/$previous_sha256sum/$sha256sum/" "$1/.SRCINFO"
