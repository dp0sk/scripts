sudo apt install cups flatpak fonts-noto-color-emoji gnome-console gnome-disk-utility gnome-shell gnome-system-monitor nautilus

sudo systemctl enable avahi-daemon.service cups.service cups-browsed.service fstrim.timer gdm.service NetworkManager.service

echo "polkit.addRule(function(action, subject) {
    if (action.id == \"org.opensuse.cupspkhelper.mechanism.all-edit\" &&
        subject.isInGroup(\"sudo\")){
        return polkit.Result.YES;
    }
});" | sudo tee /etc/polkit-1/rules.d/49-allow-passwordless-printer-admin.rules

sudo flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo

sudo flatpak install --noninteractive org.gnome.baobab org.gnome.Calculator org.gnome.Calendar org.gnome.Characters org.gnome.clocks org.gnome.Connections org.gnome.Contacts org.gnome.Evince org.gnome.font-viewer org.gnome.Epiphany org.gnome.Geary org.gnome.Logs org.gnome.Loupe org.gnome.Maps org.gnome.Music org.gnome.Photos org.gnome.SimpleScan org.gnome.Snapshot org.gnome.SoundRecorder org.gnome.TextEditor org.gnome.Totem org.gnome.Weather org.gnome.World.PikaBackup
