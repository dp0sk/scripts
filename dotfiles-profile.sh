#!/bin/bash

Real_Path()
{
	[[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#.}"
}

if [[ -f ~/.profile ]]; then
	profile=".profile"
else
	profile=".bashrc"
fi

if [[ ! -f ~/"$profile" && -f /etc/skel/"$profile" ]]; then
	cp /etc/skel/"$profile" ~/
fi

if [[ ! -f ~/.bash_logout && -f /etc/skel/.bash_logout ]]; then
	cp /etc/skel/.bash_logout ~/
fi

if [[ ! -f ~/.bash_profile && -f /etc/skel/.bash_profile ]]; then
	cp /etc/skel/.bash_profile ~/
fi

if [[ -z $(grep "HISTSIZE" ~/"$profile") ]]; then
	echo "HISTSIZE=" | tee -a ~/"$profile"
else
	sed -i 's/HISTSIZE=.*/HISTSIZE=/' ~/"$profile"
fi

if [[ -z $(grep "HISTFILESIZE" ~/"$profile") ]]; then
	echo "HISTFILESIZE=" | tee -a ~/"$profile"
else
	sed -i 's/HISTFILESIZE=.*/HISTFILESIZE=/' ~/"$profile"
fi

if [[ -z $(grep "stty -ixon" ~/"$profile") ]]; then
	echo "stty -ixon" | tee -a ~/"$profile"
fi

if [[ -z $(grep "export SCRIPTSPATH=" ~/"$profile") ]]; then
	echo "export SCRIPTSPATH=$(echo $(Real_Path "${0%/*}") | sed "s|$HOME|\$HOME|")
export PATH=\$SCRIPTSPATH:\$PATH
chmod +x \$SCRIPTSPATH/*.sh" | tee -a ~/"$profile"
fi

if [[ -z $(grep "alias rm=trash" ~/"$profile") ]]; then
	echo "alias rm=trash" | tee -a ~/"$profile"
fi

source ~/"$profile"
