#!/bin/bash

if [[ -f /var/lib/flatpak/exports/share/applications/ch.protonmail.protonmail-bridge.desktop ]]; then
	flatpak override --user --env=QT_QPA_PLATFORM=wayland ch.protonmail.protonmail-bridge
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.gnome.Music.desktop ]]; then
	flatpak override --user --unshare=network org.gnome.Music
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.gnome.Totem.desktop ]]; then
	flatpak override --user --unshare=network org.gnome.Totem
	flatpak override --filesystem=/media org.gnome.Totem --user
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.gnome.Epiphany.desktop ]]; then
	flatpak override --user --env=WEBKIT_GST_ENABLE_HLS_SUPPORT=1 org.gnome.Epiphany
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.nicotine_plus.Nicotine.desktop ]]; then
	flatpak override --user --env=NICOTINE_GTK_VERSION=4 --env=NICOTINE_LIBADWAITA=1 org.nicotine_plus.Nicotine
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.signal.Signal.desktop ]]; then
	flatpak override --user --env=SIGNAL_USE_WAYLAND=1 org.signal.Signal --socket=wayland
fi

# if [[ -f /var/lib/flatpak/exports/share/applications/org.signal.Signal.desktop ]]; then
# 	flatpak override --user --env=SIGNAL_USE_TRAY_ICON=1 org.signal.Signal
# fi
